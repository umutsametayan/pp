import React from 'react'
import PostList from '../Components/PostList'

const HomePage = () => {
  return (
    <>
      <PostList />
    </>
  )
}

export default HomePage;
