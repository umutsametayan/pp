import React from 'react';
import { useParams } from 'react-router-dom';
import PostDetail from '../Components/PostDetail';

const PostPage = () => {
  const { postId } = useParams();

  return (
    <>
      <PostDetail postId={postId} />
    </>
  );
};

export default PostPage;
