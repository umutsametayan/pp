import React from 'react';
import { useParams } from 'react-router-dom';
import CommentList from '../Components/CommentList';

const CommentPage = () => {

  const { postId } = useParams();

  return (
    <>
      <CommentList postId={postId} />
    </>
  );
};

export default CommentPage;
