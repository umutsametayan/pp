import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import App from './App.jsx';
import HomePage from './Pages/HomePage.jsx';
import PostPage from './Pages/PostPage.jsx';
import CommentPage from './Pages/CommentPage.jsx';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
    <Router>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<HomePage />} />
          <Route path="posts/:postId" element={<PostPage />} />
          <Route path="comments/:postId" element={<CommentPage />} />
        </Route>
      </Routes>
    </Router>
);
