import React from 'react';
import { useParams } from 'react-router-dom';
import useGet from '../hooks/useGet';
import CommentList from './CommentList';
import { getData } from '../api/postService'
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';

const PostDetail = () => {
  const { postId } = useParams();
  const { data: post, loading, error } = useGet(getData, `https://jsonplaceholder.typicode.com/posts/${postId}`);

  if (loading) return <Grid>Loading...</Grid>;
  if (error) return <Grid>Error loading post</Grid>;

  return (
    <>
      <Typography variant="h3">{post.title}</Typography>
      <Typography mt={2}>{post.body}</Typography>
      <Grid mt={3}>
        <Divider mt={3} />
        <Grid mt={2}>
          <Typography variant="h4">Yorumlar</Typography>
        </Grid>
      </Grid>
      <CommentList postId={postId} />
    </>
  );
};

export default PostDetail;
