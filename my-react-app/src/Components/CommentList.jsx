import React, { useState, useEffect } from 'react';
import useGet from '../hooks/useGet';
import useDelete from '../hooks/useDelete';
import usePost from '../hooks/usePost';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import usePatch from '../hooks/usePatch';
import { getData } from '../api/postService';

const CommentList = ({ postId }) => {
  const { data: initialComments, loading: commentsLoading, error: commentsError } = useGet(getData, `https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
  const { deleteData: deleteComment } = useDelete('https://jsonplaceholder.typicode.com/comments');
  const { patchData: editComment } = usePatch('https://jsonplaceholder.typicode.com/comments');
  const { data, loading, error, postApi: postComment } = usePost('https://jsonplaceholder.typicode.com/comments');

  const [comments, setComments] = useState([]);
  const [deletingComments, setDeletingComments] = useState({});
  const [editingCommentId, setEditingCommentId] = useState(null);
  const [editingCommentText, setEditingCommentText] = useState('');
  const [newComment, setNewComment] = useState('');
  const [isNew, setIsNew] = useState(false);
  const [editingComments, setEditingComments] = useState({});
  const [addingComment, setAddingComment] = useState(false);

  useEffect(() => {
    if (initialComments) {
      setComments(initialComments);
    }
  }, [initialComments]);

  const handleDelete = async (commentId) => {
    setDeletingComments(prevState => ({ ...prevState, [commentId]: true }));
    try {
      await deleteComment(commentId);
      setComments(prevComments => prevComments.filter(comment => comment.id !== commentId));
    } catch (error) {
      console.error('Error deleting comment:', error);
    } finally {
      setDeletingComments(prevState => ({ ...prevState, [commentId]: false }));
    }
  };

  const handleEdit = (comment) => {
    setEditingCommentId(comment.id);
    setEditingCommentText(comment.body);
  };

  const handleSave = async (commentId) => {
    setEditingComments(prevState => ({ ...prevState, [commentId]: true }));
    try {
      await editComment(commentId, { body: editingCommentText });
      setComments(prevComments => prevComments.map(comment =>
        comment.id === commentId ? { ...comment, body: editingCommentText } : comment
      ));
      setEditingCommentId(null);
      setEditingCommentText('');
    } catch (error) {
      console.error('Error editing comment:', error);
    } finally {
      setEditingComments(prevState => ({ ...prevState, [commentId]: false }));
    }
  };

  const cancelHandler = (x, y) => (x(), y());

  const handleNewCommentSave = async () => {
    setAddingComment(true);
    try {
      const newCommentData = { postId, body: newComment, name: "New Commenter", email: "new@commenter.com" };
      const response = await postComment(newCommentData);
      if (response && response.id) {
        response.id = Math.floor(Math.random() * 100 * 5);
        setComments(prevComments => [...prevComments, response]);
      } else {
        console.error('Error: New comment does not have an id');
      }
      setNewComment('');
      setIsNew(false);
    } catch (error) {
      console.error('Error adding new comment:', error);
    } finally {
      setAddingComment(false);
    }
  };

  if (commentsLoading) return <Grid>Loading...</Grid>;
  if (commentsError) return <Grid>Error loading comments</Grid>;

  return (
    <Grid>
      {comments.map(comment => (
        <Grid key={comment.id}>
          {editingCommentId === comment.id ? (
            <Grid>
              <Grid mt={2}>
                <TextField
                  value={editingCommentText}
                  onChange={(e) => setEditingCommentText(e.target.value)}
                />
              </Grid>
              <Grid display="flex" mt={2}>
                <Grid>
                  <Button
                    variant="outlined"
                    onClick={() => handleSave(comment.id)}
                    disabled={editingComments[comment.id]}
                  >
                    {editingComments[comment.id] ? 'Saving...' : 'Save'}
                  </Button>
                </Grid>
                <Grid ml={1}>
                  <Button variant="outlined" onClick={() => cancelHandler(setEditingCommentId(null), setEditingCommentText(''))}>Cancel</Button>
                </Grid>
              </Grid>
            </Grid>
          ) : (
            <Grid>
              <Typography variant="h5" mt={2}>{comment.name}</Typography>
              <Typography mt={2}>{comment.body}</Typography>
              <Grid mt={2} display="flex">
                <Grid>
                  <Button
                    variant="outlined"
                    onClick={() => handleDelete(comment.id)}
                    disabled={deletingComments[comment.id]}
                  >
                    {deletingComments[comment.id] ? 'Deleting...' : 'Delete'}
                  </Button>
                </Grid>
                <Grid ml={1}>
                  <Button variant="outlined" onClick={() => handleEdit(comment)}>Edit</Button>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      ))}
      <br /><br />
      {!isNew && (
        <Button variant="outlined" onClick={() => setIsNew(true)}>Add Comment</Button>
      )}
      {isNew && (
        <Grid>
          <Grid>
            <TextField
              placeholder="new comment"
              value={newComment}
              onChange={(e) => setNewComment(e.target.value)}
            />
          </Grid>
          <Grid display="flex" mt={2}>
            <Grid>
              <Button
                variant="outlined"
                onClick={handleNewCommentSave}
                disabled={addingComment}
              >
                {addingComment ? 'Saving...' : 'Save'}
              </Button>
            </Grid>
            <Grid ml={1}>
              <Button variant="outlined" onClick={() => cancelHandler(setNewComment(''), setIsNew(false))}>Cancel</Button>
            </Grid>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default CommentList;
