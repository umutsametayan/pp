import React, { useState, useEffect, useCallback } from 'react';
import useGet from '../hooks/useGet';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import PostCard from './PostCard';
import { getData } from '../api/postService'
import _ from 'lodash';

const PostList = () => {
  const { data: posts, loading, error } = useGet(getData, "https://jsonplaceholder.typicode.com/posts");
  const [val, setVal] = useState('');
  const [filteredPosts, setFilteredPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const handleChange = useCallback(
    _.debounce((value, posts) => {
      if (value.length >= 3) {
        const filtered = posts.filter(post =>
          post.title.toLowerCase().includes(value.toLowerCase())
        );
        setFilteredPosts(filtered);
      } else {
        setFilteredPosts(posts);
      }
    }, 300),
    []
  );

  useEffect(() => {
    if (loading) {
      setIsLoading(true);
    } else if (posts) {
      setFilteredPosts(posts);
      setIsLoading(false);
    }
  }, [loading, posts]);

  const onChange = e => {
    const value = e.target.value;
    setVal(value);
    handleChange(value, posts);
  };

  if (isLoading) return <Grid>Loading...</Grid>;
  if (error) return <Grid>Error loading posts</Grid>;

  return (
    <Grid>
      <Grid mt={2} mb={2}>
        <TextField fullWidth placeholder='post filtre' type="text" value={val} onChange={onChange} />
      </Grid>
      <Grid align="center" container spacing={2}>
        {filteredPosts.map(post => (
          <Grid key={post.id} item lg={3} md={6} xs={12}>
            <PostCard data={post} />
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default PostList;
