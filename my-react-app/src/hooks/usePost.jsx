// usePost.js
import { useState, useCallback } from 'react';
import { postData } from '../api/postService';

const usePost = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const postApi = useCallback(
    (postDataPayload) => {
      return postData(url, postDataPayload, setData, setLoading, setError);
    },
    [url]
  );

  return { data, loading, error, postApi };
};

export default usePost;
