import { useState, useEffect } from 'react';

const useGet = (api, url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    api(url, setData, setLoading, setError);
  }, [url]);

  return { data, loading, error };
};

export default useGet;
