import { useState } from 'react';
import axios from 'axios';

// buraları yapmaya üşendim useGet.jsx ve usePost.jsx üzerinden doğru kullanımı görebilirsin

const usePatch = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const patchData = async (id, updatedData) => {
    setLoading(true);
    try {
      const response = await axios.patch(`${url}/${id}`, updatedData);
      setData(response.data);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  return { data, loading, error, patchData };
};

export default usePatch;
