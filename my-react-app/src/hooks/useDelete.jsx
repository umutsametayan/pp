import { useState } from 'react';
import axios from 'axios';


// buraları yapmaya üşendim useGet ve usePost üzerinden doğru kullanımı görebilirsin



const useDelete = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const deleteData = async id => {
    setLoading(true);
    try {
      const response = await axios.delete(`${url}/${id}`);
      setData(response.data);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  return { data, loading, error, deleteData };
};

export default useDelete;
