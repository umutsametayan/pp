import axios from 'axios';

export const getData = async (url, setData, setLoading, setError) => {
  try {
    const response = await axios.get(url, setData);
    setData(response.data);
  } catch (err) {
    setError(err);
  } finally {
    setLoading(false);
  }
};

export const postData = async (url, postDataPayload, setData, setLoading, setError) => {
  setLoading(true);
  try {
    const response = await axios.post(url, postDataPayload);
    setData(response.data);
    return response.data;
  } catch (err) {
    setError(err);
    throw err;
  } finally {
    setLoading(false);
  }
};
